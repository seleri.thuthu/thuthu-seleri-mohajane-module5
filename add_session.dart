import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_third_project/seminars_list.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key, required String title}) : super(key: key);

  @override
  AddSessionState createState() => AddSessionState();
}

class AddSessionState extends State<AddSession> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController subjectController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController presenterController = TextEditingController();

  Future addSession() {
    final subject = subjectController.text;
    final location = locationController.text;
    final presenter = presenterController.text;

    final ref = FirebaseFirestore.instance.collection('Seminars').doc();

    return ref
        .set({
          'Seminar': subject,
          'Presenter': presenter,
          'Location': location,
          'Doc ID': ref.id
        })
        .then((value) => log('Seminar added'))
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: subjectController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    hintText: 'Enter Seminar Subject',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: locationController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: 'Enter Seminar Location',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: presenterController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    hintText: 'Enter Name of Presenter',
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  addSession();
                  subjectController.text = '';
                  locationController.text = '';
                  presenterController.text = '';

                },
                child: const Text('Add Seminar'),
              ),
            ],
          ),
          const SeminarsList()
        ]));
  }
}
