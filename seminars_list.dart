import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SeminarsList extends StatefulWidget {
  const SeminarsList({Key? key}) : super(key: key);

  @override
  State<SeminarsList> createState() => _SeminarsListState();
}

class _SeminarsListState extends State<SeminarsList> {
  final Stream<QuerySnapshot> _mySeminars =
      FirebaseFirestore.instance.collection('Seminars').snapshots();

  @override
  Widget build(BuildContext context) {
    
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();
    TextEditingController presenterController = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection('Seminars')
          .doc(docId)
          .delete()
          .then((value) => print('Deleted'));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection('Seminars');
      subjectController.text = data['Seminar'];
      locationController.text = data['Location'];
      presenterController.text = data['Presenter'];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('Update'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: subjectController,
              ),
              TextField(
                controller: locationController,
              ),
              TextField(
                controller: presenterController,
              ),
              TextButton(
                  onPressed: () {
                    collection.doc(data['Doc ID']).update({
                      'Seminar': subjectController.text,
                      'Location': locationController.text,
                      'Presenter': presenterController.text,
                    });
                    Navigator.pop(context);
                  },
                  child: const Text('Update'))
            ],
          ),
        ),
      );
    }

    return StreamBuilder(
        stream: _mySeminars,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return const Text('Something has gone wrong.');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }

          if (snapshot.hasData) {
            return Row(children: [
              Expanded(
                child: SizedBox(
                    height: (MediaQuery.of(context).size.height),
                    width: (MediaQuery.of(context).size.width),
                    child: ListView(
                      children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                          documentSnapshot.data() as Map<String, dynamic>;
                      return Column(children: [
                        Card(
                          child: Column(children: [
                            ListTile(
                              leading: Text(data['Seminar']),
                              title: Text(data['Presenter']),
                              subtitle: Text(data['Location']),
                            ),
                            ButtonTheme(
                                child: ButtonBar(
                              children: [
                                OutlinedButton.icon(
                                  onPressed: () {
                                    _update(data);
                                  },
                                  icon: const Icon(Icons.edit),
                                  label: const Text('Edit'),
                                ),
                                OutlinedButton.icon(
                                    onPressed: () {
                                      _delete(data['Doc ID']);
                                    },
                                    icon: const Icon(Icons.remove),
                                    label: const Text('Delete')),
                              ],
                            ))
                          ]),
                        ),
                      ]);
                    }).toList())),
              )
            ]);
          } else {
            return const Text('No Data');
          }
        });
  }
}
