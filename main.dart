import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:my_third_project/add_session.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyBqBAWaykS97_Ltvq7vEKDgGwBtOS6qx8E",
          authDomain: "week5project-10aa2.firebaseapp.com",
          projectId: "week5project-10aa2",
          storageBucket: "week5project-10aa2.appspot.com",
          messagingSenderId: "234631767214",
          appId: "1:234631767214:web:97d4f6f2d671f11976393f"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Trading Strategies Seminars',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage(title: 'Trading Strategies Seminars'),
      );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: 
        Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              AddSession(title: 'Trading Strategies Seminars'),
            ],
          ),
        ),
      ),
    );
  }
}
